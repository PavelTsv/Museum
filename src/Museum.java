import java.time.Clock;
import java.util.*;

/**
 * Created by pavel on 13.08.17.
 */

//vseki muzei si ima dni ot sedmicata i chasove v tezi dni v koito mojesh da go posetish bezplatno.
//na razlichnite hora im trqbva obiknovenno razlichno vreme da razgledat muzei.
//da mogat da se pusnat razlichen broi hora
//i da se vidi dali i kak kato te vlizat v muzei
//razglejdat go i izlizat moje da se smetne gore dolu kolko choveka mogat da poseshtavat muzeq.
//rezervacionna sistema, koqto da mojesh ot predniq den da zapazish che iskash da vidish muzeq
// i da znaesh dali shte mojesh i  da poluchabash tochen chas v koito da otidesh.
public class Museum {
    private String[] freeVisitDays;
    private int freeVisitHours;
    public int visitors = 0;
    private final int maxCustomers;
    static List<Museum> museums = new ArrayList<Museum>();

    //in Minutes since midnight. Maybe do it with data and time?
    private int openingHour = 480;//8 o'clock in the morning
    private int closingHour = 1320;//10 o'clock in the evening
    private int maxVisitTime = 480;//8hours. Too much maybe?
    private int minVisitTIme = 5; //5minutes


    //better way to deal with the Exceptions.
    public Museum(String[] freeVisitDays, int freeVisitHours, int maxCustomers/*capacity*/) throws Exception {
        checkForCorrectness(freeVisitDays, freeVisitHours);
        this.freeVisitDays = freeVisitDays;
        this.freeVisitHours = freeVisitHours;
        this.maxCustomers = maxCustomers;
        museums.add(this);
    }

    private void checkForCorrectness(String[] freeVisitDays, int freeVisitHours) throws Exception {
        for (String day : freeVisitDays) {
            if (!isValidDay(day)) {
                throw new Exception("Wrong day of the week. Please enter a correct one.");
            }
        }
        if (!isValidHour(freeVisitHours)) {
            throw new Exception("Incorrect hour. Please enter a correct one.");
        }
    }

    private boolean isValidDay(String workingDay) {
        String[] validDays = {"Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday"};
        boolean isValid = false;
        for (String day : validDays) {
            if (workingDay.toLowerCase() == day.toLowerCase()) {
                isValid = true;
                break;
            }
        }
        return isValid;
    }

    private boolean isValidHour(int workingHour) {
        return 8 <= workingHour && workingHour <= 22;
    }

    //Might work with enums
    enum WorkingHours {
        Monday(),
        Tuesday(),
        Wednesday(),
        Thursday(),
        Friday(),
        Saturday(),
        Sunday()
    }

    public void setFreeVisitDays() {

    }

    public int getVisitors() {
        return visitors;
    }

    public int getFreeVisitHours() {
        return freeVisitHours;
    }

    public String[] getFreeVisitDays() {
        return freeVisitDays;
    }

    public int getMuseumCount() {
        return museums.size();
    }

    public int getMaxCustomers() {
        return maxCustomers;
    }



}
