import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by pavel on 13.08.17.
 */
public class Visitor {
    private String name;
    private int age;
    private int visitHour;
    private int visitTime;
    private final String[] names = {"George","Ivar","Chris","Gwen","Lucas","Eve","Lyana","Sarah","Tris","Steve"};
    static List<Visitor> visitors = new ArrayList<Visitor>();

    public Visitor(String name, int age) {
        this.name = name;
        this.age = age;
        visitors.add(this);

    }

    /*public static void main(String[] args) {
        Museum scienceMuseum = new Museum(new String[]{"Friday","Sunday"}, 12, 20);

    }*/
    //Wrong place?
    public void visitRandomMuseums() {
        Random random = new Random();
        String visitorName;
        int visitorAge;
        int visitors = 100;
        for (int i = 0; i < visitors; i++) {
            visitorName = names[random.nextInt(names.length)];
            visitorAge = random.nextInt(100);
            this.visitors.add(new Visitor(visitorName, visitorAge));
        }
    }
    public void visitRandomMuseums(List<Museum> museums) {
        Random random = new Random();
        for(Visitor visitor : visitors) {
            visitor.visit(museums.get(random.nextInt(museums.size())));
        }
    }

    //in Minutes since midnight. Maybe do it with data and time?
    private int openingHour = 480;//8 o'clock in the morning
    private int closingHour = 1320;//10 o'clock in the evening
    private int maxVisitTime = 480;//8hours. Too much maybe?
    private int minVisitTIme = 5; //5minutes

    public void visit(Museum museum) {
        //in minutes where 5min is the min and 8hours the max visit time
        //Visitors - children,adults,old people
        //free for children and old people

        Random random = new Random();//Better way to deal with random?
        //The amount of time he wants to spend at the museum
        this.visitTime = random.nextInt(maxVisitTime) + minVisitTIme;
        this.visitHour = generateVisitHour(visitTime);
        museum.visitors++;
    }

    private int generateVisitHour(int visitTime) {
        Random random = new Random();
        int visitHour = random.nextInt(closingHour - visitTime) + openingHour;
        return visitHour;
    }


    public void reserveSpot(Museum museum) {}

}
